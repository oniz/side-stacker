import { createContext, useCallback, useContext, useEffect, useState } from "react";
import { GameMode, apiConfig, gameApi } from "../api/game-api";
import { BoardData, GameData, GameState, PlayerData, SocketResponse, StartGameResponse } from "../data/game-data";

interface GameContextData {
    gameState: GameState;
    game: GameData;
    board: BoardData;
    player: PlayerData;
    createGame: (mode: GameMode) => Promise<boolean>;
    joinGame: (gameCode: string) => Promise<boolean>;
    addPiece: (row: number, isLeft: boolean) => Promise<boolean>;
    resetGame: () => Promise<boolean>;
}

const GameContext = createContext<GameContextData>({} as GameContextData);

let ws: WebSocket;

export function GameProvider({ children }: React.PropsWithChildren) {
    const [game, setGame] = useState<GameData>({
        id: 0,
        code: "",
        board: "",
        mode: GameMode.computer,
        playerTurn: null,
        finished: 0,
        winner: null,
        winningMove: null,
    });
    const [board, setBoard] = useState<BoardData>([]);
    const [player, setPlayer] = useState<PlayerData>({
        id: 0,
        gameId: 0,
        code: "",
    });
    const [gameState, setGameState] = useState(GameState.mainMenu);
    
    useEffect(() => {
        const intervalId = setInterval(() => {
            if (gameState === GameState.mainMenu || gameState === GameState.ended) {
                return;
            }
            
            if (ws?.readyState !== WebSocket.CLOSED) {
                ws?.send(JSON.stringify({
                    type: "ping",
                    player,
                }));
            }
        }, 250);
        
        return () => {
            clearInterval(intervalId);
        };
    }, [gameState, player, game.playerTurn]);
    
    const handleMessage = useCallback((message: string) => {
        const obj: SocketResponse = JSON.parse(message);
        if (obj.game) {
            setGame(obj.game);
            setBoard(JSON.parse(obj.game.board));
            if (obj.game.finished) {
                setGameState(GameState.ended);
                ws.close();
            }
            else if (obj.players?.length === 2) {
                setGameState(GameState.playing);
            }
        }
    }, []);
    
    const createGame = async (mode: GameMode) => {
        if (gameState !== GameState.mainMenu) {
            return false;
        }
        
        const res = await gameApi.createGame(mode);
        return finishGameSetup(res, mode, false);
    };
    
    const joinGame = async (gameCode: string) => {
        if (gameState !== GameState.mainMenu) {
            return false;
        }
        
        const res = await gameApi.joinGame(gameCode);
        return finishGameSetup(res, GameMode.multiplayer, true);
    };
    
    const finishGameSetup = (res: StartGameResponse, mode: GameMode, isJoining: boolean) => {
        if (!res.success) {
            return false;
        }
        
        setGame(res.game!);
        setBoard(JSON.parse(res.game!.board));
        setPlayer(res.player!);
        
        const config = apiConfig();
        ws = new WebSocket(`${config.wsScheme}://${config.baseUrl}/ws`);
        ws.addEventListener("open", () => {
            if (isJoining) {
                setGameState(GameState.playing);
            }
            else {
                if (mode === GameMode.computer) {
                    setGameState(GameState.playing);
                }
                else {
                    setGameState(GameState.awaitingPlayerJoin);
                }
            }
        });
        ws.addEventListener("message", (event) => {
            handleMessage(event.data);
        });
        return true;
    };
    
    const addPiece = async (row: number, isLeft: boolean) => {
        if (gameState !== GameState.playing) {
            return false;
        }
        
        ws.send(JSON.stringify({
            type: "addPiece",
            player,
            row,
            isLeft,
        }));
        return true;
    };
    
    const resetGame = async () => {
        setGameState(GameState.mainMenu);
        return true;
    };
    
    const value: GameContextData = {
        gameState,
        game,
        board,
        player,
        createGame,
        joinGame,
        addPiece,
        resetGame,
    };
    
    return (
        <GameContext.Provider value={value}>
            {children}
        </GameContext.Provider>
    );
}

export const useGameContext = () => useContext(GameContext);