import { AddPieceMessage, GameData, PlayerData } from "./data";

const emptyBoard = '[[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]]';

export async function createNewGame(env: Env, mode: string): Promise<GameData> {
	const code = Date.now().toString(32) + Math.random().toString(32).substring(2);
	const ret = await env.DB.prepare("INSERT INTO game (code, board, mode) VALUES (?, ?, ?) RETURNING id")
		.bind(code, emptyBoard, mode)
		.all();
	const record = ret.results[0];

	return {
		id: record["id"] as number,
		code,
		board: emptyBoard,
		mode,
		playerTurn: null,
		finished: 0,
		winner: null,
		winningMove: null,
	};
}

export async function createNewPlayer(env: Env, gameId: number): Promise<PlayerData> {
	const code = Date.now().toString(32) + Math.random().toString(32).substring(2);
	const pingtime = Date.now();
	const ret = await env.DB.prepare("INSERT INTO player (gameId, code, pingtime) VALUES (?, ?, ?) RETURNING id")
		.bind(gameId, code, pingtime)
		.all();
	const record = ret.results[0];

	return {
		id: record["id"] as number,
		gameId,
		code,
		disconnected: 0,
		pingtime,
	};
}

export async function updateGame(env: Env, game: GameData) {
	await env.DB.prepare("UPDATE game SET board = ?, playerTurn = ?, finished = ?, winner = ?, winningMove = ? WHERE id = ?")
		.bind(game.board, game.playerTurn, game.finished, game.winner, game.winningMove, game.id)
		.run();
}

export async function updatePlayerPing(env: Env, player: PlayerData) {
	await env.DB.prepare("UPDATE player SET pingtime = ? WHERE id = ?")
		.bind(Date.now(), player.id)
		.run();
}

export async function addGameMove(env: Env, msg: AddPieceMessage) {
	await env.DB.prepare("INSERT INTO game_move (gameId, time, playerId, position) VALUES (?, ?, ?, ?)")
		.bind(msg.player.gameId, Date.now(), msg.player.id, `${msg.row}-${msg.isLeft ? "L" : "R"}`)
		.run();
}

export async function getOtherGamePlayerId(env: Env, gameId: number, excludePlayerId: number): Promise<number | null> {
	const ret = await env.DB.prepare("SELECT id FROM player WHERE gameId = ? AND id <> ?")
		.bind(gameId, excludePlayerId)
		.all();
	if (ret.results.length) {
		return ret.results[0]["id"] as number;
	}

	return null;
}

export async function getGameFromPlayer(env: Env, player: PlayerData): Promise<GameData | null> {
	const gameRes = await env.DB.prepare("SELECT g.* FROM player p INNER JOIN game g ON g.id = p.gameId WHERE p.id = ? AND p.code = ? AND p.gameId = ?")
		.bind(player.id, player.code, player.gameId)
		.all();
	if (gameRes.results.length === 0) {
		return null;
	}

	return gameRes.results[0] as any;
}

export async function getGameFromCode(env: Env, code: string): Promise<GameData | null> {
	const gameRes = await env.DB.prepare("SELECT * FROM game WHERE code = ? AND mode = 'multiplayer'")
		.bind(code)
		.all();
	if (gameRes.results.length === 0) {
		return null;
	}

	return gameRes.results[0] as any;
}

export async function getPlayersFromGame(env: Env, gameId: number): Promise<PlayerData[] | null> {
	const playerRes = await env.DB.prepare("SELECT * FROM player WHERE gameId = ?")
		.bind(gameId)
		.all();
	if (playerRes.results.length === 0) {
		return null;
	}

	return playerRes.results as any;
}
