import { WinResult } from "./data/data";

export function addCors(res: Response) {
	res.headers.set("Access-Control-Allow-Origin", "*");
	res.headers.set("Access-Control-Allow-Methods", "GET,HEAD,POST,OPTIONS");
	res.headers.set("Access-Control-Max-Age", "86400");
	return res;
}
export const COMPUTER_PLAYER_ID = -1;
export const EMPTY_ITEM = 0;

const matchDeltas = [
    [[0,0],[1,0],[2,0],[3,0]],
    [[0,0],[0,1],[0,2],[0,3]],
    [[0,0],[1,1],[2,2],[3,3]],
    [[0,0],[-1,1],[-2,2],[-3,3]],
    [[0,0],[1,-1],[2,-2],[3,-3]],
];

export function checkBoardWin(board: number[][]): WinResult {
	for (let y = 0; y < board.length; ++y) {
		for (let x = 0; x < board[y].length; ++x) {
			for (const delta of matchDeltas) {
				const positions = [
					[x + delta[0][0], y + delta[0][1]],
					[x + delta[1][0], y + delta[1][1]],
					[x + delta[2][0], y + delta[2][1]],
					[x + delta[3][0], y + delta[3][1]],
				];
				const item1 = board[positions[0][1]]?.[positions[0][0]];
				const item2 = board[positions[1][1]]?.[positions[1][0]];
                const item3 = board[positions[2][1]]?.[positions[2][0]];
                const item4 = board[positions[3][1]]?.[positions[3][0]];
				if ((item1 === item2 && item2 === item3 && item3 === item4) && item1 !== EMPTY_ITEM && item1 !== undefined) {
					return {
						win: true,
						playerId: item1,
						positions,
					};
				}
			}
		}
	}

	return {
		win: false,
		playerId: 0,
		positions: [],
	};
}

export function isBoardFull(board: number[][]): boolean {
	for (let y = 0; y < board.length; ++y) {
		const row = board[y];
		if (row.find(x => x === EMPTY_ITEM) !== undefined) {
			return false;
		}
	}

	return true;
}
