import { useState } from "react";
import { GameMode } from "../api/game-api";
import { useGameContext } from "../context/game-context";
import { GameState } from "../data/game-data";
import "./Header.scss";

export function Header() {
    const [errorMessage, setErrorMessage] = useState("");
    const [joinCode, setJoinCode] = useState("");
    const { createGame, resetGame, joinGame, gameState, game, player } = useGameContext();
    
    const handleStartComputerGame = async () => {
        const success = await createGame(GameMode.computer);
        setErrorMessage(success ? "" : "Could not start game");
    };
    
    const handleStartMultiplayerGame = async () => {
        const success = await createGame(GameMode.multiplayer);
        setErrorMessage(success ? "" : "Could not start game");
    };
    
    const handleJoinGame = async () => {
        const success = await joinGame(joinCode);
        setErrorMessage(success ? "" : "Could not join game");
    };
    
    const handleReset = async () => {
        resetGame();
        setJoinCode("");
    };
    
    const renderStateName = () => {
        switch (gameState) {
            case GameState.mainMenu:
                return (
                    <>
                        <h1>Main Menu</h1>
                        <hr />
                        <div>
                            <button onClick={handleStartComputerGame}>Start Computer Game</button>
                            <button onClick={handleStartMultiplayerGame}>Start Multiplayer Game</button>
                        </div>
                        <hr />
                        <div>
                            <label htmlFor="game-code">Join Game Code:</label>
                            <input id="game-code" type="text" value={joinCode} onChange={(e) => setJoinCode(e.target.value)} />
                            <button onClick={handleJoinGame}>Join</button>
                        </div>
                    </>
                );
            case GameState.awaitingPlayerJoin:
                return (
                    <>
                        <h1>Waiting for Other Player</h1>
                        <h3>Give them the code: {game?.code}</h3>
                    </>
                );
            case GameState.playing:
                return (
                    <>
                        <h1>Playing</h1>
                        {
                            game.playerTurn === player.id
                            ? <h3>Your turn</h3>
                            : <h3>Opponent's turn</h3>
                        }
                    </>
                );
            case GameState.ended:
                return (
                    <>
                        <h1>Game Over</h1>
                        <h3>{game.winner === player.id ? "You Win" : (game.winner === null ? "It's a Tie" : "You Lose")}</h3>
                        <button onClick={handleReset}>Reset</button>
                    </>
                );
        }
    }
    
    return (
        <header>
            <div>
                {renderStateName()}
            </div>
            <div className="error">
                {errorMessage}
            </div>
        </header>
    );
}