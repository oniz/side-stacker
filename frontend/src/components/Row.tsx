import { useGameContext } from "../context/game-context";
import { GameState } from "../data/game-data";
import "./Row.scss";

interface RowProps {
    index: number;
    row: number[];
}

export function Row({ index, row }: RowProps) {
    const { gameState, game: { playerTurn }, player, addPiece } = useGameContext();
    const isMyTurn = (playerTurn === player.id) && (gameState === GameState.playing);
    
    const clickLeft = () => {
        if (isMyTurn) {
            addPiece(index, true);
        }
    };
    
    const clickRight = () => {
        if (isMyTurn) {
            addPiece(index, false);
        }
    };
    
    const renderPiece = (piece: number) => {
        if (piece === 0) {
            return null;
        }
        
        return (
            piece === player.id
            ? <div className="piece red"></div>
            : <div className="piece blue"></div>
        );
    }
    
    return (
        <div className="board-row">
            <button className={`click-cell left ${isMyTurn ? "active" : ""}`} onClick={clickLeft}>&nbsp;</button>
            {
                row.map((x, i) => (
                    <div key={i} className="cell">
                        {renderPiece(x)}
                    </div>
                ))
            }
            <button className={`click-cell right ${isMyTurn ? "active" : ""}`} onClick={clickRight}>&nbsp;</button>
        </div>
    );
}