import { AddPieceMessage, GameData, PingMessage, ResponseMessage } from "./data/data";
import { addGameMove, getGameFromPlayer, getOtherGamePlayerId, updateGame, updatePlayerPing } from "./data/queries";
import { COMPUTER_PLAYER_ID, EMPTY_ITEM, checkBoardWin, isBoardFull } from "./utils";

export async function handleMessage(message: any, env: Env, server: WebSocket) {
	if (message.type === "addPiece") {
		const msg = message as AddPieceMessage;
		const game = await getGameFromPlayer(env, msg.player);
		if (!game || game.finished || game.playerTurn !== msg.player.id) {
			server.send(JSON.stringify({
				success: false,
				game,
			}));
			return;
		}

		const res = await handleAddPiece(env, msg, game);
		server.send(JSON.stringify(res));
	}
	else if (message.type === "ping") {
		const msg = message as PingMessage;
		const game = await getGameFromPlayer(env, msg.player);
		if (!game || game.finished) {
			server.send(JSON.stringify({
				success: false,
				game,
			}));
			return;
		}

		if (game.playerTurn === COMPUTER_PLAYER_ID && !game.finished) {
			await makeComputerMove(env, game);
		}

		await updatePlayerPing(env, msg.player);
		const res: ResponseMessage = {
			success: true,
			game,
		};

		if (game.mode === "multiplayer") {
			const otherPlayerId = await getOtherGamePlayerId(env, game.id, msg.player.id);
			if (otherPlayerId) {
				res.players = [otherPlayerId, msg.player.id];
			}
		}
		server.send(JSON.stringify(res));
	}
}

async function handleAddPiece(env: Env, msg: AddPieceMessage, game: GameData): Promise<ResponseMessage> {
	const board = JSON.parse(game.board);
	const moveRes = performMove(board, msg);
	if (!moveRes) {
		return {
			success: true,
			game,
		};
	}
	game.board = JSON.stringify(board);
	const winner = checkBoardWin(board);
	if (winner.win) {
		game.winner = winner.playerId;
		game.finished = 1;
		game.winningMove = JSON.stringify(winner.positions);
	}
	else if (isBoardFull(board)) {
		game.finished = 1;
	}

	if (!game.finished) {
		if (game.mode !== "computer" || game.playerTurn === COMPUTER_PLAYER_ID) {
			game.playerTurn = await getOtherGamePlayerId(env, game.id, game.playerTurn ?? 0);
		}
		else {
			game.playerTurn = COMPUTER_PLAYER_ID;
		}
	}

	await Promise.all([
		updateGame(env, game),
		addGameMove(env, msg),
	]);

	return {
		success: true,
		game,
	};
}

function performMove(board: number[][], msg: AddPieceMessage): false | number[][] {
	const row = board[msg.row];
	const zeroIndex = row.indexOf(0);
	if (zeroIndex === -1) {
		return false;
	}
	row.splice(zeroIndex, 1);
	if (msg.isLeft) {
		row.splice(0, 0, msg.player.id);
	}
	else {
		row.push(msg.player.id);
	}
	return board;
}

async function makeComputerMove(env: Env, game: GameData) {
	const board: number[][] = JSON.parse(game.board);
	const length = board.length;
	const startRowIndex = Math.ceil(Math.random() * length);
	for (let i = startRowIndex; i < startRowIndex + length; ++i) {
		const rowIndex = i % length;
		const row = board[rowIndex];
		if (row.find(x => x === EMPTY_ITEM) === EMPTY_ITEM) {
			const msg: AddPieceMessage = {
				type: "addPiece",
				row: rowIndex,
				isLeft: Math.random() > 0.5,
				player: {
					id: COMPUTER_PLAYER_ID,
					gameId: game.id,
					code: "",
					disconnected: 0,
					pingtime: 0,
				},
			};
			return await handleAddPiece(env, msg, game);
		}
	}
}
