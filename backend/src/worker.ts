/**
 * Welcome to Cloudflare Workers! This is your first worker.
 *
 * - Run `npm run dev` in your terminal to start a development server
 * - Open a browser tab at http://localhost:8787/ to see your worker in action
 * - Run `npm run deploy` to publish your worker
 *
 * Learn more at https://developers.cloudflare.com/workers/
 */

import { handleMessage } from './message-handler';
import apiRouter from './router';
import { addCors } from './utils';

export default {
	// The fetch handler is invoked when this worker receives a HTTP(S) request
	// and should return a Response (optionally wrapped in a Promise)
	async fetch(request: Request, env: Env, ctx: ExecutionContext): Promise<Response> {
		const url = new URL(request.url);

		if (url.pathname.startsWith('/api/')) {
			// You can also use more robust routing
			return apiRouter.handle(request, env);
		}
		else if (request.headers.get("Upgrade") === "websocket") {
			const webSocket = startWebsocket(env);

			return addCors(new Response(null, {
				status: 101,
				webSocket,
			}));
		}

		return addCors(new Response(
			`Invalid request: ${request.url}`,
			{ headers: { 'Content-Type': 'text/html' } }
		));
	},
};

function startWebsocket(env: Env) {
	const { 0: client, 1: server } = new WebSocketPair();
	server.accept();
	server.addEventListener("message", (event) => {
		handleMessage(JSON.parse(event.data as string), env, server);
	});

	return client;
}
