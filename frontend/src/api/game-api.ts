import { StartGameResponse } from "../data/game-data";

export const enum GameMode {
    computer = "computer",
    multiplayer = "multiplayer",
}

export function apiConfig() {
    if (window.location.hostname === "localhost") {
        return {
            baseUrl: "localhost:8787",
            httpScheme: "http",
            wsScheme: "ws",
        };
    }
    else {
        return {
            baseUrl: "side-stacker-backend.cflare-072.workers.dev",
            httpScheme: "https",
            wsScheme: "wss",
        };
    }
}

export const gameApi = {
    async createGame(mode: GameMode): Promise<StartGameResponse> {
        const config = apiConfig();
        const url = `${config.httpScheme}://${config.baseUrl}/api/new-game`
        try {
            const res = await fetch(url, {
                method: "POST",
                body: JSON.stringify({ mode }),
            })
            .then(res => res.json());
            return res;
        }
        catch (ex) {
            console.error(ex);
            return {
                success: false,
                game: null,
                player: null,
            };
        }
    },
    async joinGame(gameCode: string): Promise<StartGameResponse> {
        const config = apiConfig();
        const url = `${config.httpScheme}://${config.baseUrl}/api/join-game`
        try {
            const res = await fetch(url, {
                method: "POST",
                body: JSON.stringify({ gameCode }),
            })
            .then(res => res.json());
            return res;
        }
        catch (ex) {
            console.error(ex);
            return {
                success: false,
                game: null,
                player: null,
            };
        }
    },
};
