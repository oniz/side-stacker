export interface GameData {
	id: number;
	code: string;
	board: string;
	mode: string;
	playerTurn: number | null;
	winner: number | null;
	winningMove: string | null;
	finished: number;
}

export interface PlayerData {
	id: number;
	gameId: number;
	code: string;
	disconnected: number;
	pingtime: number;
}

export interface GameMoveData {
	id: number;
	gameId: number;
	time: number;
	playerId: number;
	position: string;
}

export interface AddPieceMessage {
	type: "addPiece";
	player: PlayerData;
	row: number;
	isLeft: boolean;
}

export interface PingMessage {
	type: "ping";
	player: PlayerData;
}

export interface WinResult {
	win: boolean;
	playerId: number;
	positions: number[][];
}

export interface ResponseMessage {
	success: boolean;
	message?: string;
	game?: GameData;
	players?: number[];
}
