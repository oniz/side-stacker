import { Router } from 'itty-router';
import { PlayerData } from './data/data';
import { createNewGame, createNewPlayer, getGameFromCode, getPlayersFromGame, updateGame } from './data/queries';
import { addCors } from './utils';

// now let's create a router (note the lack of "new")
const router = Router();

// POST to the collection (we'll use async here)
router.post('/api/new-game', async (request, env: Env) => {
	try {
		const content = await request.json();
		const mode = content["mode"];
		const game = await createNewGame(env, mode);
		const player = await createNewPlayer(env, game.id);
		if (mode === "computer") {
			game.playerTurn = player.id;
			await updateGame(env, game);
		}

		const res = {
			success: true,
			game,
			player,
		};
		return addCors(new Response(JSON.stringify(res)));
	}
	catch (ex) {
		console.error(ex);
		return failResponse(ex as any);
	}
});

router.post('/api/join-game', async (request, env: Env) => {
	try {
		const content = await request.json();
		const code = content["gameCode"];
		const game = await getGameFromCode(env, code);
		if (!game) {
			console.log("No multiplayer game for code", code);
			return failResponse();
		}
		let player: PlayerData | undefined;
		const existingPlayers = await getPlayersFromGame(env, game.id);

		if (existingPlayers && existingPlayers.length === 2) {
			const time = Date.now() - 3000;
			player = existingPlayers.find(x => x.pingtime < time);
		}

		if (!player) {
			if (existingPlayers && existingPlayers.length < 2) {
				player = await createNewPlayer(env, game.id);
			}
			else {
				return failResponse("No more than 2 players can be in a game");
			}
		}

		if (!game.playerTurn) {
			game.playerTurn = player.id;
		}
		await updateGame(env, game);

		const res = {
			success: true,
			game,
			player,
		};
		return addCors(new Response(JSON.stringify(res)));
	}
	catch (ex) {
		console.error(ex);
		return failResponse(ex as any);
	}
});

// 404 for everything else
router.all('*', () => new Response('Not Found.', { status: 404 }));

function failResponse(msg?: string) {
	return addCors(new Response("Fail response; " + msg ?? "", { status: 501 }));
}
export default router;
