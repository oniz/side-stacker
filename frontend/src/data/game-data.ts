export enum GameState {
    mainMenu = "mainMenu",
    awaitingPlayerJoin = "awaitingPlayerJoin",
    playing = "playing",
    ended = "ended",
}

export type BoardData = number[][];

export interface GameData {
	id: number;
	code: string;
	board: string;
	mode: string;
	playerTurn: number | null;
	winner: number | null;
	winningMove: string | null;
	finished: number;
}

export interface PlayerData {
	id: number;
	gameId: number;
	code: string;
}

export interface StartGameResponse {
    success: boolean;
    game: GameData | null;
    player: PlayerData | null;
}

export interface WinResult {
	playerId: number;
	position: number[];
	path: number[][];
}

export interface SocketResponse {
	success: boolean;
	message?: string;
	game?: GameData;
	players?: number[];
}