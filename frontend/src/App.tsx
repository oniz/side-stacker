import "./App.css";
import { Board } from "./components/Board";
import { Header } from "./components/Header";
import { GameProvider } from "./context/game-context";

export function App() {
	return (
		<GameProvider>
			<main>
				<Header />
				<Board />
			</main>
		</GameProvider>
	)
}
