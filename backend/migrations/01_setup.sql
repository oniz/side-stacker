CREATE TABLE game (
	id INTEGER PRIMARY KEY,
	code TEXT UNIQUE NOT NULL,
	board TEXT NOT NULL,
	mode TEXT NOT NULL,
	playerTurn INTEGER,
	winner INTEGER,
	winningMove TEXT,
	finished INTEGER NOT NULL DEFAULT 0
);

CREATE TABLE player (
	id INTEGER PRIMARY KEY,
	gameId INTEGER REFERENCES game(id),
	code TEXT NOT NULL
);
CREATE INDEX ix_player_game ON player(gameId);

CREATE TABLE game_move (
	id INTEGER PRIMARY KEY,
	gameId INTEGER REFERENCES game(id),
	time INTEGER NOT NULL,
	playerId INTEGER NOT NULL,
	position TEXT NOT NULL
);
CREATE INDEX ix_game_move_game ON game_move(gameId);

