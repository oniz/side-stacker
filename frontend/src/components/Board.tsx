import { useGameContext } from "../context/game-context";
import { GameState } from "../data/game-data";
import "./Board.scss";
import { Row } from "./Row";

export function Board() {
    const { board, gameState } = useGameContext();
    
    if (gameState === GameState.mainMenu) {
        return null;
    }
    
    return (
        <div className="board">
            {
                board.map((row, index) => (
                    <Row key={index} index={index} row={row} />
                ))
            }
        </div>
    );
}